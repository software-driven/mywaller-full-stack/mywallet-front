import React, { useState } from 'react'

/* link */
import { Link } from 'react-router-dom'

/* react-router-dom */
import { useNavigate } from 'react-router-dom'

/* api */
import api from '../../services/api'

/* alert */
import Alert from '../../components/Alert'

/* Components */ 
import Button from '../../components/Button'

/* styled */
import { PageSignin, PageSignupForm, PageSignupTitle } from './styled'



const Signup: React.FC = () => {

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [message, setMessage] = useState({ show: false, alert: '' })

  const navigation = useNavigate()

  const handleSignup = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      await api.post('/signup', { name, email, password, password_confirm: confirmPassword })
      navigation('/')
    } catch(error: any) {
      setMessage({
        alert: error?.response?.data?.error,
        show: true,
      })
    }
  }

  return (
    <PageSignin>
       <Alert show={message.show} type="error" setshowAlert={setMessage}>
       {message.alert}
      </Alert>
      <PageSignupForm>
        <PageSignupTitle>MyWallet</PageSignupTitle>
        <form className="form" method="post" onSubmit={handleSignup}>
          <input type="name" name="text" required placeholder="Nome" value={name} onChange={e => setName(e.currentTarget.value)} />
          <input type="email" name="email" required placeholder="E-mail" value={email} onChange={e => setEmail(e.currentTarget.value)} />
          <input type="password" name="password" required placeholder="Senha" value={password} onChange={e => setPassword(e.currentTarget.value)} />
          <input type="password" name="password" required placeholder="Confirme a senha" value={confirmPassword} onChange={e => setConfirmPassword(e.currentTarget.value)} />
          <Button>Cadastrar</Button>
        </form>
        <Link to="/" className="signup-link">Já tem uma conta? Entre Agora!</Link>
      </PageSignupForm>
    </PageSignin>
  )
}

export default Signup