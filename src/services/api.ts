import axios from 'axios'

axios.defaults.baseURL = 'https://moneytran.herokuapp.com/'

export default axios